//
//  main.m
//
//  Created by xiayang on 13-2-13.
//  Copyright (c) 2013年 eoe.cn. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        /*******************************************************************************************
         NSMutableArray
         *******************************************************************************************/
        /*--------------- 给数组分配容量----------------*/
        NSArray *array;
        array = [NSMutableArray arrayWithCapacity:20];
        
        
        
        /*-------------- 在数组末尾添加对象----------------*/
        //- (void) addObject: (id) anObject;
        //NSMutableArray *array = [NSMutableArray arrayWithObjects:@"One",@"Two",@"Three",nil];
        [array addObject:@"Four"];
        NSLog(@"array:%@",array);
        
        
        
        /*-------------- 删除数组中指定索引处对象----------------*/
        //-(void) removeObjectAtIndex: (unsigned) index;
        //NSMutableArray *array = [NSMutableArray arrayWithObjects:@"One",@"Two",@"Three",nil];
        [array removeObjectAtIndex:1];
        NSLog(@"array:%@",array);
        
        
        
        /*------------- 数组枚举---------------*/
        //- (NSEnumerator *)objectEnumerator;从前向后
        //NSMutableArray *array = [NSMutableArray arrayWithObjects:@"One",@"Two",@"Three",nil];
        NSEnumerator *enumerator;
        enumerator = [array objectEnumerator];
        
        id thingie;
        while (thingie = [enumerator nextObject]) {
            NSLog(@"thingie:%@",thingie);
        }
        
        
        //- (NSEnumerator *)reverseObjectEnumerator;从后向前
        //NSMutableArray *array = [NSMutableArray arrayWithObjects:@"One",@"Two",@"Three",nil];
        //NSEnumerator *enumerator;
        //enumerator = [array reverseObjectEnumerator];
        
        id object;
        while (object = [enumerator nextObject]) {
            NSLog(@"object:%@",object);
        }
        
        
        //快速枚举
        //NSMutableArray *array = [NSMutableArray arrayWithObjects:
        //@"One",@"Two",@"Three",nil];
        for(NSString *string in array)
        {
            NSLog(@"string:%@",string);
        }
        
        
        
        /*******************************************************************************************
         NSDictionary
         *******************************************************************************************/
        
        /*------------------------------------创建字典 ------------------------------------*/
        //- (id) initWithObjectsAndKeys;
        
        NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"One",@"1",@"Two",@"2",@"Three",@"3",nil];
        NSString *string = [dictionary objectForKey:@"One"];
        NSLog(@"string:%@",string);
        NSLog(@"dictionary:%@",dictionary);
        [dictionary release];
        
        
        /*******************************************************************************************
         NSMutableDictionary
         *******************************************************************************************/
        
        /*------------------------------------创建可变字典 ------------------------------------*/
        //创建
        //NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        
        //添加字典
        [dictionary setObject:@"One" forKey:@"1"];
        [dictionary setObject:@"Two" forKey:@"2"];
        [dictionary setObject:@"Three" forKey:@"3"];
        [dictionary setObject:@"Four" forKey:@"4"];
        NSLog(@"dictionary:%@",dictionary);
        
        //删除指定的字典
        [dictionary removeObjectForKey:@"3"];
        NSLog(@"dictionary:%@",dictionary);
        
        
        /*******************************************************************************************
         NSValue（对任何对象进行包装）
         *******************************************************************************************/
        
        /*--------------------------------将NSRect放入NSArray中 ------------------------------------*/
        //将NSRect放入NSArray中
        //NSMutableArray *array = [[NSMutableArray alloc] init];
        NSValue *value;
        CGRect rect = CGRectMake(0, 0, 320, 480);
        value = [NSValue valueWithBytes:&rect objCType:@encode(CGRect)];
        [array addObject:value];
        NSLog(@"array:%@",array);
        
        //从Array中 提取
        value = [array objectAtIndex:0];
        [value getValue:&rect];
        NSLog(@"value:%@",value);
        
        
        /*******************************************************************************************
         从目录搜索扩展名为jpg的文件
         *******************************************************************************************/
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSString *home;
        home = @"../Users/";
        
        NSDirectoryEnumerator *direnum;
        direnum = [fileManager enumeratorAtPath: home];
        
        NSMutableArray *files = [[NSMutableArray alloc] init];
        
        //枚举
        NSString *filename;
        while (filename = [direnum nextObject]) {
            if([[filename pathExtension] hasSuffix:@"jpg"]){
                [files addObject:filename];
            }
        }
        //快速枚举
        //for(NSString *filename in direnum)
        //{
        //    if([[filename pathExtension] isEqualToString:@"jpg"]){
        //        [files addObject:filename];
        //    }
        //}
        NSLog(@"files:%@",files);
        
        //枚举
        NSEnumerator *filenum;
        filenum = [files objectEnumerator];
        while (filename = [filenum nextObject]) {
            NSLog(@"filename:%@",filename);
        }
        
    }
    return 0;
}
